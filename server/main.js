import { Meteor } from 'meteor/meteor';
import { Links } from '../imports/collections/links'

import { WebApp } from 'meteor/webapp';
import ConnectRoute from 'connect-route';

Meteor.startup(() => {
  Meteor.publish('links', function() {
    return Links.find({});
  });
});

// Executed whenever a user visits with a route like
//'localhost:3000/abcd'
function onRoute(req, res, next) {
  //take the token out of the url and try to find a
  //matching link in the Links collections
  const link = Links.findOne({ token: req.params.token });

  //if we find a link object, redirect the user to the
  //long url
  if(link){
    Links.update(link, { $inc: { clicks: 1} });
    res.writeHead(307, { 'Location': link.url });
    res.end();
  }else{
    //if we don't find a link object, send the user
    //to our normal React App
    next();
  }

}

// localhost:3000/ NO MATCH
// localhost:3000/books/harry_potter NO MATCH
// localhost:3000/abcd will match!!

const middleware = ConnectRoute( function(router) {
  router.get('/:token', onRoute);
});

//Adding a meteor middleware
WebApp.connectHandlers.use(middleware);
