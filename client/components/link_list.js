import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data'; //new
import { Links } from '../../imports/collections/links';

class LinkList extends Component {
  renderRows(){
    return this.props.links.map(link => {
      const { url, clicks, token } = link;
      const kidLink = `http://localhost:3000/${token}`;

      return (
        <tr key={token} >
          <td>{url}</td>
          <td>
            <a href={kidLink}> {kidLink} </a>
          </td>
          <td>{clicks}</td>
        </tr>
      );
    });
  }

  render() {
    return(
      <table className="table">
        <thead>
          <tr>
            <th>URL</th>
            <th>Adress</th>
            <th>Clicks</th>
          </tr>
        </thead>
        <tbody>
          {this.renderRows()}
        </tbody>
      </table>
    );
  }
}

//container, tracker de datos
export default withTracker(() => {
 Meteor.subscribe('links');

 return { links: Links.find({}).fetch() };
})(LinkList);
